### README.md

# RefCard-01 Spring Boot Application

## Was soll mit diesem Projekt erreicht werden?
RefCard-01 is a web application that allows developers and network administrators to dynamically display IP addresses on a simple static website. This project aims to demonstrate the use of Docker for building and running a Spring Boot application in an isolated environment, preventing version conflicts and dependency issues.

## Kann ich es benutzen?
### Voraussetzungen
- **Betriebssystem**: Linux, macOS oder Windows
- **Framework Infrastruktur**: Docker, Java Development Kit (JDK) 17
- **Wissen**: Grundkenntnisse in Git, Maven und Docker, Grundkenntnisse in Spring Boot und Java

## Wie kann ich es installieren?
### 1. Projekt klonen
Zuerst klonen Sie das Projekt in Ihr lokales Verzeichnis:
```bash
cd ~
git clone https://gitlab.com/bbwrl/m347-ref-card-01
cd m347-ref-card-01
```

### 2. Dockerfile erstellen
Erstellen Sie eine `Dockerfile` im Projektverzeichnis mit folgendem Inhalt:
```Dockerfile
# base image mit maven/eclipse/jdk17
FROM maven:3.9-eclipse-temurin-17-alpine

# Kopiere die beiden Codequellen
COPY pom.xml .
COPY src src

# maven build prozess
RUN mvn package

# Container Start-Befehl
ENTRYPOINT ["java","-jar","target/app-refcard-01-0.0.1-SNAPSHOT.jar"]
```

### 3. Docker Image erstellen und Container starten
Erstellen Sie das Docker Image und starten Sie den Container:
```bash
docker build -t app:latest .
docker run -p 8080:8080 app
```

### 4. Anwendung im Browser aufrufen
Rufen Sie die Anwendung im Browser unter `http://localhost:8080` auf. Ersetzen Sie `localhost` entsprechend Ihrer Umgebung, falls erforderlich.

### Code-Snippets, die sofort funktionieren
- **Projekt klonen und in das Verzeichnis wechseln**:
    ```bash
    cd ~
    git clone https://gitlab.com/bbwrl/m347-ref-card-01
    cd m347-ref-card-01
    ```

- **Docker Image erstellen und Container starten**:
    ```bash
    docker build -t app:latest .
    docker run -p 8080:8080 app
    ```
